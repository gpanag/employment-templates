## For People Managers

1. [ ] People Experience: Update the direct reports in BambooHR to offboarded team member's manager.  
1. [ ] @gitlab-com/people-group/peopleops-eng: Check if they had nominations they had not approved yet for their direct and indirect (second level) reports.

