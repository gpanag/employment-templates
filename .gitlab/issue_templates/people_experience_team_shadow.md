# Purpose:
The purpose of the PEA Shadow Program is to allow any team member to get a better understanding of what the role looks like and all processes involved on a daily basis. This is also a great way to promote collaboration and transparency across all teams. The shadower is encouraged to share any suggestions along the way on how things could be improved to create a better experience for all.

# Topics:

Onboarding
Offboarding
Career Mobility
Probation Period
General Queries (Slack and by email)
Anniversary gifts 
Every day gifts and flowers


# Structure:

## Pre Shadow Program Tasks: 

1. [ ] Shadower: Comment your top three objectives for taking part in this program. 
1. [ ] Shadower: Complete the [GitLab Tools 101 Training](https://about.gitlab.com/handbook/people-group/learning-and-development/certifications/gitlab-101/)
1. [ ] Shadower: Ensure that you are subscribed to the `#people-connect` & `#pea-team` Slack channel. 
1. [ ] Shadower: All People Experience team process can be found in the Handbook, consider bookmarking this [page](https://about.gitlab.com/handbook/people-group/people-experience-team/). 

## Week 1 - 2:

### General Enquiries
1. [ ] Shadower: Read up on the People Experience Associate responsibilities in the [Handbook](https://about.gitlab.com/job-families/people-ops/people-experience-associate/).
1. [ ] Shadower: Read up on what the People Experience Responsibilities and Timelines are in the [Handbook](https://about.gitlab.com/handbook/people-group/#people-experience-vs-people-operations-core-responsibilities--response-timeline)
1. [ ] Shadower: Go through all of the tasks reflected in the Responsibilities and Timelines page to get a better understanding of each task.
1. [ ] Shadower: Schedule a 30 minute Zoom call with the People Experience Associate to shadow any email and Slack queries. 
1. [ ] People Experience Associate: Invite the shadower to our Experience Team meeting for the weeks that they are shadowing, as well as any other calls you feel will be beneficial to the shadower. 
1. [ ] People Experience Associate: Share the PEA Training folder in Google Drive with the Shadower
1. [ ] People Experience Associate: Schedule a 30 minute call with the shadower for any questions that they may have throughout the day.
1. [ ] People Experience Associate: Set some time aside to allow the shadower see the steps required when moderating a Group Call. If you are hosting a Group Call, please ask the shadower to join you on the call prior to the start time. 
1. [ ] People Experience Associate: Consider tagging the shadower on different `Merge Requests` and `Issues` to allow them to get exposure to the different changes and discussion our team is involved in. This also encourages additional collaboration.  

### Onboarding Training  
1. [ ] Shadower: Read the onboardings page available in the [Handbook](https://about.gitlab.com/handbook/people-group/general-onboarding/)
1. [ ] Shadower: Read the onboarding processes page available in the [Handbook](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/)
1. [ ] Shadower: Watch the video training available in the "Shadower Training Videos" shared Google Drive. 
1. [ ] Shadower: Schedule a 30 minute call with the People Experience Associate to go through the onboarding workflow and ask any questions. 
1. [ ] People Experience Associate: When on the call with the shadower, try and share as much context as to why things are done a certain way during the pre-onboarding and onboarding process. Answer any questions the shadower may have. 

### Offboarding Training  
1. [ ] Shadower: Read the offboardings page available in the [Handbook](https://about.gitlab.com/handbook/people-group/offboarding/)
1. [ ] Shadower: Read the offboarding guidelines page available in the [Handbook](https://about.gitlab.com/people-group/handbook/offboarding/offboarding_guidelines/)
1. [ ] Shadower: Watch the video training available in the "Shadower Training Videos" shared Google Drive. 
1. [ ] Shadower: Schedule a 30 minute call with the People Experience Associate to go through the offboarding workflow and ask any questions. 
1. [ ] People Experience Associate: When on the call with the shadower, try and share as much context as to why things are done a certain way and what impact there would be if not actioned. Answer any questions the shadower may have. 

### Career Mobility and Probation Period 
1. [ ] Shadower: Read the Promotions and Transfers page available in the [Handbook](https://about.gitlab.com/handbook/people-group/promotions-transfers/)
1. [ ] Shadower: Read about probationary periods on the page available in the [Handbook](https://about.gitlab.com/handbook/people-group/contracts-and-international-expansion/#probation-period)
1. [ ] Shadower: Watch the video training available in the "Shadower Training Videos" shared Google Drive for the career mobility and probation period process. 
1. [ ] Shadower: Schedule a 30 minute call with the People Experience Associate to go through the career mobility and probation period workflow and ask any questions. 
1. [ ] People Experience Associate: When on the call with the shadower, try and share as much context as to why things are done a certain way. Answer any questions the shadower may have. 

### Overview
1. [ ] Shadower: Review all topics covered throughout the week and set aside time with the People Experience Associate to ask any additional questions. 
1. [ ] Shadower: Use today as an opportunity to provide feedback to the People Experience Associate. 
1. [ ] Shadower: Consider connecting with previous shadow participants for any guidance and advice. 

## Week 2 - 4:

Use the time to starting practicing live examples of the training implemented.
1. [ ] Shadower: Use today as an opportunity to provide feedback to the People Experience Associates. 
1. [ ] Shadower: Add yourself to the [People Experience Shadow Program page](https://about.gitlab.com/handbook/people-group/people-experience-shadow-program/#previous-shadow-program-joiners), along with your GitLab handle and month you participated in. 

Shadow People Exp Associate to get better understanding of compliance and audits required.
1. [ ] Shadower: Read up about the People Experience Associate responsibilities in the [Handbook](https://about.gitlab.com/job-families/people-ops/people-experience-associate/)
1. [ ] People Experience Associate: Schedule time with the shadower to go through the importance of compliance and auditing
1. [ ] People Experience Associate: Schedule time with the shadower to go through the weekly audit process for the following:
        - Onboarding Issues
        - Offboarding Issues
        - Career Mobility Issues
        - Probation Periods
        - Referral Bonus 

Shadow Senior, People Experience Associate
1. [ ] Shadower: Read up about the Sr. People Experience Associate responsibilities in the [Handbook](https://about.gitlab.com/job-families/people-ops/people-experience-associate/)
1. [ ] Sr. People Experience Associate: Schedule time with the shadower to go through your role and key focus areas. 
1. [ ] Sr. People Experience Associate: Schedule time with the shadower at the end of the program for a feedback session.  
